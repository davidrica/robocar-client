import React, {Component} from 'react';
import {
    Container, Header, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch, Spinner, 
    Row, Col, Fab, View
} from 'native-base';
import Slider from '@react-native-community/slider';
import io from 'socket.io-client';
import { WebView } from 'react-native-webview';
import styles from './Style';

const connection = io('ws://id11.tunnel.my.id:1366/');

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rightSpeed: 0,
            rightDirection: false,
            leftSpeed: 0,
            leftDirection: false,
            direction: false,
            isConnectionActive: false,
            initialStateLoaded: false,
            active: false
        };
    }

    componentDidMount() {
        connection.on('connect', () => {
            console.log('connect')
            this.setState({
                isConnectionActive: true,
            });
        });

        connection.on('open', () => {
            console.log('open')
            this.setState({
                isConnectionActive: true,
            });
        });

        connection.on('disconnect', () => {
            this.setState({
                isConnectionActive: false,
            });
        });

        connection.on('update-gpio-state', (data) => {
            //get pin status from server
            // console.log('dataToggle: ', data);
            this.setState({
                rightSpeed: data.rightSpeed,
                rightDirection: data.rightDirection,
                leftSpeed: data.leftSpeed,
                leftDirection: data.leftDirection,
                initialStateLoaded: true,
            });
        });
    }

    changeDirection(color) {
        const {
            isConnectionActive,
            rightSpeed,
            rightDirection,
            leftSpeed,
            leftDirection,
            direction
        } = this.state;
        if (!isConnectionActive) {
            alert('Server connection is closed!');
        }

        // change button state
        if (color == 'right') {
            this.setState({rightDirection: !rightDirection}, () => this.socketEvent());
        }
        if (color == 'left') {
            this.setState({leftDirection: !leftDirection}, () => this.socketEvent());
        }
        if (color == 'forward') {
            if (direction) {
                this.setState({direction: false, leftDirection: false, rightDirection: false}, () => this.socketEvent());
            } else {
                this.setState({direction: true, leftDirection: true, rightDirection: true}, () => this.socketEvent());
            }
        }
    }

    socketEvent() {
        const { rightSpeed, rightDirection, leftSpeed, leftDirection, } = this.state;
        // emit `update-gpio-state` socket event
        connection.emit('update-gpio-state', {
            rs: rightSpeed,
            rd: rightDirection,
            ls: leftSpeed,
            ld: leftDirection,
        });
    }

    onThrottle(val) {
        const { rightDirection, leftDirection } = this.state;
        this.setState({rightSpeed: Math.floor(val), leftSpeed: Math.floor(val)}, () => this.socketEvent());
        console.log('Right: ', Math.floor(val), rightDirection ? 'Forward' : 'Reverse');
        console.log('Left: ', Math.floor(val), leftDirection ? 'Forward' : 'Reverse');
    }

    onRightThrottle(val) {
        const { rightDirection } = this.state;
        this.setState({rightSpeed: Math.floor(val)}, () => this.socketEvent());
        console.log('Right: ', Math.floor(val), rightDirection ? 'Forward' : 'Reverse');
    }

    onLeftThrottle(val) {
        const { leftDirection } = this.state;
        this.setState({leftSpeed: Math.floor(val)}, () => this.socketEvent());
        console.log('Left: ', Math.floor(val), leftDirection ? 'Forward' : 'Reverse');
    }

    render(props) {
        const {isConnectionActive, initialStateLoaded} = this.state;
        if (false) {
            return (
                <Container>
                    <Text>Halo</Text>
                    <WebView source={{ uri: 'http://id11.tunnel.my.id:1366/' }} />
                </Container>
            )
        } else {
        return (
            <Container>
                <WebView style={styles.webview} source={{ uri: 'http://id11.tunnel.my.id:1366/' }} />
                <View style={{  }}>
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{ }}
                        style={{ backgroundColor: '#EC3C3D' }}
                        position="bottomRight"
                        onPress={() => this.setState({ active: !this.state.active })}>
                        <Icon name="add-outline" />
                        {/* <Button style={{ backgroundColor: '#34A34F' }}>
                        <Icon name="logo-whatsapp" />
                        </Button>
                        <Button style={{ backgroundColor: '#3B5998' }}>
                        <Icon name="logo-facebook" />
                        </Button>
                        <Button disabled style={{ backgroundColor: '#DD5144' }}>
                        <Icon name="mail" />
                        </Button> */}
                    </Fab>
                    </View>
                {
                    this.state.active &&
                    <Content>
                        <ListItem icon>
                            <Left>
                                <Button style={{backgroundColor: '#F92223'}}>
                                    <Icon active name="sunny-outline" />
                                </Button>
                            </Left>
                            <Body>
                                <Text>Right Forward</Text>
                            </Body>
                            <Right>
                                {isConnectionActive && initialStateLoaded ? (
                                    <Switch
                                        onValueChange={() => this.changeDirection('right')}
                                        value={this.state.rightDirection}
                                    />
                                ) : (
                                    <Spinner />
                                )}
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{backgroundColor: '#43D751'}}>
                                    <Icon active name="sunny-outline" />
                                </Button>
                            </Left>
                            <Body>
                                <Text>Left Forward</Text>
                            </Body>
                            <Right>
                                {isConnectionActive && initialStateLoaded ? (
                                    <Switch
                                        onValueChange={() => this.changeDirection('left')}
                                        value={this.state.leftDirection}
                                    />
                                ) : (
                                    <Spinner />
                                )}
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{backgroundColor: '#007AFF'}}>
                                    <Icon active name="sunny-outline" />
                                </Button>
                            </Left>
                            <Body>
                                <Text>Forward</Text>
                            </Body>
                            <Right>
                                {isConnectionActive && initialStateLoaded ? (
                                    <Switch
                                        onValueChange={() => this.changeDirection('forward')}
                                        value={this.state.direction}
                                    />
                                ) : (
                                    <Spinner />
                                )}
                            </Right>
                        </ListItem>
                        <ListItem>
                            
                        </ListItem>
                        <Row style={styles.ph20}>
                            <Col>
                                <Text>Left</Text>
                                <Slider
                                    style={{width: 200, height: 40}}
                                    minimumValue={90}
                                    maximumValue={255}
                                    minimumTrackTintColor="#DDDDDD"
                                    maximumTrackTintColor="#000000"
                                    vertical={true}
                                    onValueChange={(val) => this.onLeftThrottle(val)}
                                />
                            </Col>
                            <Col>
                                <Text>Right</Text>
                                <Slider
                                    style={{width: 200, height: 40}}
                                    minimumValue={90}
                                    maximumValue={255}
                                    minimumTrackTintColor="#DDDDDD"
                                    maximumTrackTintColor="#000000"
                                    vertical={true}
                                    onValueChange={(val) => this.onRightThrottle(val)}
                                />
                            </Col>
                        </Row>

                        <Row style={styles.ph20}>
                            <Col>
                                <Text>Throttle</Text>
                                <Slider
                                    style={{width: 200, height: 40}}
                                    minimumValue={90}
                                    maximumValue={255}
                                    minimumTrackTintColor="#DDDDDD"
                                    maximumTrackTintColor="#000000"
                                    vertical={true}
                                    onValueChange={(val) => this.onThrottle(val)}
                                />
                            </Col>
                        </Row>
                        <Row>
                        {/* <WebView
                            scalesPageToFit={false}
                            bounces={false}
                            javaScriptEnabled={false}
                            style={{ height: 240, width: '100%', marginBottom: 50 }}
                            source={{
                                html: `<iframe src="http://id11.tunnel.my.id:1366/" 
                                title="iframe Example 1" width="100%" height="600"></iframe>`
                            }}
                            automaticallyAdjustContentInsets={true}
                            /> */}
                        {/* <WebView source={{ uri: 'http://id11.tunnel.my.id:1366/' }} /> */}
                        </Row>
                    </Content>
                }
            </Container>
        );}
    }
}

export default App;
